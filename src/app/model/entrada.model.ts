export interface IEntrada {
    operation: string;
    unitcost: number;
    quantity: number;
}