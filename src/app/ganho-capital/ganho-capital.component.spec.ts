import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IEntrada } from '../model/entrada.model';

import { GanhoCapitalComponent } from './ganho-capital.component';

describe('GanhoCapitalComponent', () => {
    let component: GanhoCapitalComponent;
    let fixture: ComponentFixture<GanhoCapitalComponent>;
    let entrada: IEntrada[] = [];

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [GanhoCapitalComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(GanhoCapitalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('1 Teste RealizaCalculo', () => {
        entrada = [
            { operation: "buy", unitcost: 10, quantity: 100 },
            { operation: "sell", unitcost: 15, quantity: 50 },
            { operation: "sell", unitcost: 15, quantity: 50 }
        ];
        let saida = [{"tax": 0},{"tax": 0},{"tax": 0}];
        const result = component.realizaCalculo(entrada);

        expect(result).toEqual(saida);
    })

    it('2 Teste RealizaCalculo', () => {
        entrada = [
            { operation: "buy", unitcost: 10, quantity: 10000 },
            { operation: "sell", unitcost: 20, quantity: 5000 },
            { operation: "sell", unitcost: 5, quantity: 5000 }
        ];
        const result = component.realizaCalculo(entrada);
        let saida = [{"tax": 0},{"tax": 10000},{"tax": 0}];
        expect(result).toEqual(saida);
    })

    it('3 Teste RealizaCalculo', () => {
        entrada = [
            { operation: "buy", unitcost: 10, quantity: 10000 },
            { operation: "sell", unitcost: 5, quantity: 5000 },
            { operation: "sell", unitcost: 20, quantity: 5000 }
        ];
        const result = component.realizaCalculo(entrada);
        let saida = [{"tax": 0},{"tax": 0},{"tax": 5000}];
        expect(result).toEqual(saida);
    })

    it('4 Teste RealizaCalculo', () => {
        entrada = [
            { operation: "buy", unitcost: 10, quantity: 10000 },
            { operation: "buy", unitcost: 25, quantity: 5000 },
            { operation: "sell", unitcost: 15, quantity: 10000 }
        ];
        const result = component.realizaCalculo(entrada);
        let saida = [{"tax": 0},{"tax": 0},{"tax": 0}];
        expect(result).toEqual(saida);
    })

    it('5 Teste RealizaCalculo', () => {
        entrada = [
            { operation: "buy", unitcost: 10, quantity: 10000 },
            { operation: "buy", unitcost: 25, quantity: 5000 },
            { operation: "sell", unitcost: 15, quantity: 10000 },
            { operation: "sell", unitcost: 25, quantity: 5000 }
        ];
        const result = component.realizaCalculo(entrada);
        let saida = [{"tax": 0},{"tax": 0},{"tax": 0},{"tax": 10000}]
        expect(result).toEqual(saida);
    })

    it('6 Teste RealizaCalculo', () => {
        entrada = [
            { operation: "buy", unitcost: 10, quantity: 10000 },
            { operation: "sell", unitcost: 2, quantity: 5000 },
            { operation: "sell", unitcost: 20, quantity: 2000 },
            { operation: "sell", unitcost: 20, quantity: 2000 },
            { operation: "sell", unitcost: 25, quantity: 1000 }
        ];
        const result = component.realizaCalculo(entrada);
        let saida = [{"tax": 0},{"tax": 0},{"tax": 0},{"tax": 0},{"tax": 3000}];
        expect(result).toEqual(saida);
    })

    it('1 Teste calculaPrecoMedio', () => {
        entrada = [
            { operation: "buy", unitcost: 10, quantity: 100 },
            { operation: "sell", unitcost: 15, quantity: 50 },
            { operation: "sell", unitcost: 15, quantity: 50 }
        ];
        let saida = 10
        const result = component.calculaPrecoMedio(entrada);
        expect(result).toEqual(saida);
    })
    
    it('2 Teste calculaPrecoMedio', () => {
        entrada = [
            { operation: "buy", unitcost: 10, quantity: 10000 },
            { operation: "sell", unitcost: 20, quantity: 5000 },
            { operation: "sell", unitcost: 5, quantity: 5000 }
        ];
        const result = component.calculaPrecoMedio(entrada);
        let saida = 10
        expect(result).toEqual(saida);
    })
    
     it('3 Teste calculaPrecoMedio', () => {
         entrada = [
             { operation: "buy", unitcost: 10, quantity: 10000 },
             { operation: "sell", unitcost: 5, quantity: 5000 },
             { operation: "sell", unitcost: 20, quantity: 5000 }
         ];
         const result = component.calculaPrecoMedio(entrada);
         let saida = 10
         expect(result).toEqual(saida);
     })
    
    it('4 Teste calculaPrecoMedio', () => {
        entrada = [
            { operation: "buy", unitcost: 10, quantity: 10000 },
            { operation: "buy", unitcost: 25, quantity: 5000 },
            { operation: "sell", unitcost: 15, quantity: 10000 }
        ];
        const result = component.calculaPrecoMedio(entrada);
        let saida = 15
        expect(result).toEqual(saida);
    })
    
    it('5 Teste calculaPrecoMedio', () => {
        entrada = [
            { operation: "buy", unitcost: 10, quantity: 10000 },
            { operation: "buy", unitcost: 25, quantity: 5000 },
            { operation: "sell", unitcost: 15, quantity: 10000 },
            { operation: "sell", unitcost: 25, quantity: 5000 }
        ];
        const result = component.calculaPrecoMedio(entrada);
        let saida = 15
        expect(result).toEqual(saida);
    })
    
    it('6 Teste calculaPrecoMedio', () => {
        entrada = [
            { operation: "buy", unitcost: 10, quantity: 10000 },
            { operation: "sell", unitcost: 2, quantity: 5000 },
            { operation: "sell", unitcost: 20, quantity: 2000 },
            { operation: "sell", unitcost: 20, quantity: 2000 },
            { operation: "sell", unitcost: 25, quantity: 1000 }
        ];
        const result = component.calculaPrecoMedio(entrada);
        let saida = 10
        expect(result).toEqual(saida);
    })
});
