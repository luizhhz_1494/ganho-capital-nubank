import { Component, OnInit } from '@angular/core';
import { IEntrada } from '../model/entrada.model';
import { ISaida } from '../model/saida.model';

@Component({
    selector: 'app-ganho-capital',
    templateUrl: './ganho-capital.component.html',
    styleUrls: ['./ganho-capital.component.css']
})
export class GanhoCapitalComponent implements OnInit {

    entrada: IEntrada[] = [];
    constructor() {

    }

    ngOnInit(): void {
        
    }

    realizaCalculo(entrada: IEntrada[]) {
        let taxSaida: number[] = [];
        let prejuizo: number = 0;
        let precoMedio = this.calculaPrecoMedio(entrada);

        entrada.forEach(x => {
            //Operação de compra nao paga imposto
            if (x.operation === "buy") {
                taxSaida.push(0)
                prejuizo = (prejuizo) + ((x.unitcost - precoMedio) * x.quantity)

                //Prejuízos acontecem quando você vende ações a um valor menor do que o preço médio ponderado de compra
            } else if (x.operation === "sell" && (x.unitcost < precoMedio)) {
                //Não paga Imposto
                taxSaida.push(0)
                //Calculando o Prejuizo
                prejuizo = (prejuizo) + ((x.unitcost - precoMedio) * x.quantity)

                //Você não paga nenhum imposto se o valor total da operação (custo unitário da ação x quantidade) for menor ou igual a R$ 20000
            } else if ((x.unitcost * x.quantity) <= 20000) {
                //Não paga Imposto
                taxSaida.push(0);
                //Calculando o Prejuizo
                prejuizo = (prejuizo) + ((x.unitcost - precoMedio) * x.quantity)

                //O percentual de imposto pago é de 20% sobre o lucro obtido. Ou seja, o imposto vai ser
                //pago quando há uma operação de venda cujo preço é superior ao preço médio ponderado
                //de compra.
            } else if (x.operation === "sell" && x.unitcost > precoMedio) {
                prejuizo = (prejuizo) + ((x.unitcost - precoMedio) * x.quantity)
                if (prejuizo * 0.2 > 0) {
                    taxSaida.push(prejuizo * 0.2);
                    if (prejuizo > 0) {
                        prejuizo = 0
                    }
                } else {
                    taxSaida.push(0);
                }

                //Se o preço medio ponderado é igual ao custo que eu estou posicionado, não houve prejuizo nem lucro
            } else if (x.unitcost === precoMedio) {
                taxSaida.push(0);

                //Repasando o prejuizo para o lucro futuro
            } else {
                prejuizo = (prejuizo) + ((x.unitcost - precoMedio) * x.quantity)
                if (x.unitcost - prejuizo) {
                    taxSaida.push(x.unitcost - prejuizo)
                    if (prejuizo > 0) {
                        prejuizo = 0
                    }
                }else {
                    taxSaida.push(0);
                }
            }            
        })       
        let ret = taxSaida.map( item => {
            return {tax:item};
        });
        return ret;
    }

    calculaPrecoMedio(entradaPM: IEntrada[]) {

        let precMedioCompra = 0;
        let mediaPondCompra = 0;

        //Preço medio é a soma da multiplicacao da quantidade e valor unitariogi tpush
        //divido pela soma da quantidade
        entradaPM.forEach(x => {
            if (x.operation === "buy") {
                precMedioCompra = (precMedioCompra) + (x.quantity * x.unitcost)
                mediaPondCompra = (mediaPondCompra) + x.quantity;
            }
        });

        return precMedioCompra / mediaPondCompra
    }



}
