## Instalando Angular
Realizar o dowload do node no link `https://nodejs.org/en/download/`
Executar o comando `npm install` na pasta raiz do projeto `ganho-capital-nubank`

## Iniciando o Servidor
Para iniciar o servidor, deveremos navegar até a pasta `app` podendo-se utilizar os comandos `cd src` depois `cd app` e logo na sequência rodar o comando `npm serve`

## Build
Para gerar o build deverá rodar o comando `npm run build` na pasta raiz do projeto `ganho-capital-nubank`. Os artefatos de construção serão armazenados no diretório `dist/`.

## Running unit tests
Execute na pasta raiz do projeto `ganho-capital-nubank` o comando`npm test` para executar os testes de unidade via [Karma](https://karma-runner.github.io).


